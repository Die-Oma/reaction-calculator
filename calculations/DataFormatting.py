class DataFormatting:

    def array_to_matrix(self, reaction_matrix, species_list):
        formatted_matrix = []

        for row in reaction_matrix:
            left_side = []
            right_side = []

            for i, number in enumerate(row):
                if number < 0:
                    number *= -1
                    if number == 1:
                        left_side.append(species_list[i])
                    else:
                        merged = str(number) + " " + species_list[i]
                        left_side.append(merged)

                elif number > 0:
                    if number == 1:
                        right_side.append(species_list[i])
                    else:
                        merged = str(number) + " " + species_list[i]
                        right_side.append(merged)

            both_sides = [left_side] + [right_side]
            formatted_matrix.append(both_sides)

        return formatted_matrix

    def create_list(self, input_string):
        species_list = []
        for i, symbol in enumerate(input_string):
            if i == 0:
                if symbol.isalpha() or symbol.isdigit():
                    species_list.append(symbol)

            elif symbol.isalpha() or symbol.isdigit():
                if input_string[i - 1].isalpha() or input_string[i - 1].isdigit():
                    species_list[-1] += symbol
                else:
                    species_list.append(symbol)

        for i, molecule in enumerate(species_list):
            species_list[i] = molecule.lstrip("1234567890")

        return species_list

    def create_list_for_array(self, input_array):
        matrix_list = []
        row = []

        for i, symbol in enumerate(input_array):

            if i == 0:
                if symbol.isdigit():
                    row.append(symbol)

            elif symbol == "]":
                matrix_list.append(row)
                row = []

            elif symbol == "-":
                row.append(symbol)

            elif symbol.isdigit():
                if input_array[i - 1].isdigit() or input_array[i - 1] == "-":
                    row[-1] += symbol
                else:
                    row.append(symbol)

        return matrix_list
