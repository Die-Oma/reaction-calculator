class MoleculeComparer:
    # periodic table
    periodic_table_row_1 = ["H", "He"]

    periodic_table_row_2 = ["Li", "Be", "B", "C", "N", "O", "F", "Ne"]

    periodic_table_row_3 = ["Na", "Mg", "Al", "Si", "P", "S", "Cl", "Ar"]

    periodic_table_row_4 = ["K", "Ca", "Sc", "Ti", "V", "Cr", "Mn", "Fe", "Co", "Ni", "Cu", "Zn",
                            "Ga", "Ge", "As", "Se", "Br", "Kr"]

    periodic_table_row_5 = ["Rb", "Sr", "Y", "Zr", "Nb", "Mo", "Tc", "Ru", "Rh", "Pd", "Ag", "Cd",
                            "In", "Sn", "Sb", "Te", "I", "Xe"]

    periodic_table_row_6 = ["Cs", "Ba", "La", "Ce", "Pr", "Nd", "Pm", "Sm", "Eu", "Gd", "Tb", "Dy", "Ho", "Er", "Tm",
                            "Yb",
                            "Lu", "Hf", "Ta", "W", "Re", "Os", "Ir", "Pt", "Au", "Hg",
                            "Tl", "Pb", "Bi", "Po", "At", "Rn"]

    periodic_table_row_7 = ["Fr", "Ra", "Ac", "Th", "Pa", "U", "Np", "Pu", "Am", "Cm", "Bk", "Cf", "Es", "Fm", "Md",
                            "No",
                            "Lr", "Rf", "Db", "Sg", "Bh", "Hs", "Mt", "Ds", "Rg", "Cn",
                            "Nh", "Fl", "Mc", "Lv", "Ts", "Og"]

    periodic_table = periodic_table_row_1 + periodic_table_row_2 + periodic_table_row_3 + periodic_table_row_4 \
                     + periodic_table_row_5 + periodic_table_row_6 + periodic_table_row_7

    def molecule_checker(self, species_list):
        not_in_list = []
        sliced_list = []
        for species in species_list:
            list_atoms = []
            for i, atom in enumerate(species):
                if not atom.isdigit():
                    if atom.isupper() or len(list_atoms) == 0:
                        list_atoms.append(atom)
                    else:
                        digit_test = species[i-1]
                        if digit_test.isdigit():
                            list_atoms.append(atom)
                        else:
                            list_atoms[-1] += atom

            sliced_list += list_atoms

        sliced_list = list(set(sliced_list))

        for element in sliced_list:
            if element not in self.periodic_table:
                not_in_list.append(element)

        return not_in_list
