import numpy as np
import re


class KeyReactionCalculator:

    def slice_atoms(self, species):
        sliced_atoms = []
        for elements in species:

            if elements.isupper():
                sliced_atoms.append(elements)

            elif elements.islower():
                sliced_atoms[-1] += elements

            elif elements.isdigit():
                sliced_atoms.append(elements)

        return sliced_atoms

    def list_atoms(self, sliced_atoms):
        found_atoms = []
        for sliced_element in sliced_atoms:
            if not sliced_element.isdigit():
                found_atoms.append(sliced_element)

        found_atoms = list(set(found_atoms))

        return found_atoms

    def remove_atoms(self, species_list, atom_list):
        atoms_to_remove = []
        for atom in atom_list:
            if atom in species_list:
                atoms_to_remove.append(atom)

        for atom_to_remove in atoms_to_remove:
            atom_list.remove(atom_to_remove)

        return atom_list

    def fill_enhanced_matrix(self, enhanced_reaction_matrix, species_list, sliced_species_list, enhanced_species_list):
        slice_index = [0]
        cumulated_length = 0
        for species in species_list:
            string_check = re.sub('[a-z]', '', species)
            current_length = len(string_check)
            slice_index.append(current_length + cumulated_length)
            cumulated_length += current_length

        for i, x in enumerate(slice_index):
            if not i + 1 < len(slice_index):
                continue

            sliced_molecule = sliced_species_list[x:slice_index[i + 1]]
            for j, element in enumerate(sliced_molecule):
                if element.isdigit():
                    continue

                molecule_index = enhanced_species_list.index(element)

                if j + 1 < len(sliced_molecule):
                    if sliced_molecule[j + 1].isdigit():
                        number_check = sliced_molecule[j+1:]
                        atom_number = "0"
                        for number in number_check:
                            if number.isdigit():
                                atom_number += number
                            else:
                                break

                        enhanced_reaction_matrix[i][molecule_index] = int(atom_number) * -1
                    else:
                        enhanced_reaction_matrix[i][molecule_index] = -1
                else:
                    enhanced_reaction_matrix[i][molecule_index] = -1

        return enhanced_reaction_matrix

    def linear_combination(self, enhanced_reaction_matrix, rows, matrix_position):
        matrix_row = matrix_position[0]
        matrix_column = matrix_position[1]
        matrix_value1 = enhanced_reaction_matrix[matrix_row, matrix_column]
        if matrix_row + 1 == rows:
            enhanced_reaction_matrix = np.delete(enhanced_reaction_matrix, matrix_row, axis=0)
            return enhanced_reaction_matrix

        for i in range(matrix_row + 1, rows):
            matrix_value2 = enhanced_reaction_matrix[i, matrix_column]

            if i == rows - 1 and matrix_value2 == 0:
                enhanced_reaction_matrix = np.delete(enhanced_reaction_matrix, matrix_row, axis=0)
                return enhanced_reaction_matrix

            if matrix_value2 != 0:
                enhanced_reaction_matrix = self.subtract_row(enhanced_reaction_matrix, matrix_row, i, matrix_value1,
                                                             matrix_value2)
                return enhanced_reaction_matrix

    def calculate_linear_combination(self, enhanced_reaction_matrix, new_atoms, enhanced_species_list):
        atom_columns = []
        for atom in new_atoms:
            rows, _ = enhanced_reaction_matrix.shape
            position_atom = enhanced_species_list.index(atom)
            atom_columns.append(position_atom)

            for row in range(rows):
                matrix_element = enhanced_reaction_matrix[row, position_atom]
                matrix_position = [row, position_atom]
                if matrix_element != 0:
                    enhanced_reaction_matrix = self.linear_combination(enhanced_reaction_matrix, rows, matrix_position)

                row_check, _ = enhanced_reaction_matrix.shape
                if row_check < rows:
                    break

        enhanced_reaction_matrix = np.delete(enhanced_reaction_matrix, atom_columns, axis=1)

        return enhanced_reaction_matrix

    def remove_atoms_matrix(self, atom_list, new_atoms, reaction_matrix, species_list):
        atom_rows = []
        atoms_in_species = list(set(atom_list) - set(new_atoms))

        for atom in atoms_in_species:
            atom_rows.append(species_list.index(atom))

        reaction_matrix = np.delete(reaction_matrix, atom_rows, axis=0)

        return reaction_matrix

    def calculate_gauss(self, reaction_matrix):
        rows, columns = reaction_matrix.shape
        for row in range(rows):
            for row2 in range(row + 1, rows):
                matrix_value_1 = reaction_matrix[row, row2]
                matrix_value_2 = reaction_matrix[row2, row2]
                if not matrix_value_1 == 0 or not matrix_value_2 == 0:
                    reaction_matrix = self.subtract_row(reaction_matrix, row, row2, matrix_value_1, matrix_value_2)

        return reaction_matrix

    def subtract_row(self, matrix, row_position_1, row_position_2, matrix_value_1, matrix_value_2):
        row1 = matrix[row_position_1, :] * matrix_value_2
        row2 = matrix[row_position_2, :] * matrix_value_1
        row1 = row1 - row2
        matrix[row_position_1, :] = row1
        return matrix

    def divide_row(self, matrix):
        rows, _ = matrix.shape
        for row in range(rows):
            matrix_row = matrix[row, :]
            divider = np.gcd.reduce(matrix_row)

            if matrix[row, row] < 0:
                matrix_row = matrix_row * -1

            matrix[row, :] = matrix_row / divider
        return matrix

    def calc(self, species_list):

        sliced_species_list = []

        for species in species_list:
            sliced_species_list += self.slice_atoms(species)

        atom_list = self.list_atoms(sliced_species_list)

        new_atoms = self.remove_atoms(species_list, atom_list.copy())

        enhanced_species_list = species_list + new_atoms

        enhanced_reaction_matrix = np.eye(len(species_list), len(enhanced_species_list), dtype=int)

        self.fill_enhanced_matrix(enhanced_reaction_matrix, species_list, sliced_species_list, enhanced_species_list)

        if len(atom_list) > len(new_atoms):
            enhanced_reaction_matrix = self.remove_atoms_matrix(atom_list, new_atoms, enhanced_reaction_matrix,
                                                                species_list)

        reaction_matrix = self.calculate_linear_combination(enhanced_reaction_matrix, new_atoms, enhanced_species_list)

        reaction_matrix = self.calculate_gauss(reaction_matrix)

        reaction_matrix = self.divide_row(reaction_matrix)

        reaction_matrix = reaction_matrix.tolist()

        return reaction_matrix
