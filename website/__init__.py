import os

from flask import Flask
from dotenv import load_dotenv

def create_app():
    app = Flask(__name__)

    load_dotenv()
    app.config["SECRET_KEY"] = os.environ.get('secretKey')

    from .views import views

    app.register_blueprint(views, url_prefix="/")

    return app