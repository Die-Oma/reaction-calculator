from flask import Blueprint, render_template, request, flash
import flask_excel as excel

from calculations.DataFormatting import DataFormatting
from calculations.KeyReactionCalculator import KeyReactionCalculator
from calculations.MoleculeComparer import MoleculeComparer

krc = KeyReactionCalculator()
mc = MoleculeComparer()
df = DataFormatting()


views = Blueprint("views", __name__)
excel.init_excel(views)


@views.route("/")
def home():
    return render_template("home.html")


@views.route("/prodcalc", methods=["GET", "POST"])
def product_calculation():
    if request.method == "POST":
        products = request.form.get("products")
        product_list = df.create_list(products)
        reactants = request.form.get("reactants")
        reactant_list = df.create_list(reactants)
        species_list = product_list + reactant_list
        no_atoms = mc.molecule_checker(species_list)

        if len(product_list) < 1:
            flash('There are no products in the List', category="error")
            return render_template("productcalculation.html",
                                   reactants=reactants,
                                   reactionMatrix="none")
        if len(reactant_list) < 1:
            flash('There are no reactants in the List', category="error")
            return render_template("productcalculation.html",
                                   products=products,
                                   reactionMatrix="none")

        elif len(no_atoms) > 0:
            alert_string = "The following entries are no atoms: "
            atom_string = ", ".join(no_atoms)
            alert_string += atom_string
            flash(alert_string, category="error")
            return render_template("productcalculation.html",
                                   products=products,
                                   reactants=reactants,
                                   reactionMatrix="none")

        else:
            key_matrix = krc.calc(species_list)
            formatted_key_matrix = df.array_to_matrix(key_matrix, species_list)
            flash('Calculation successful', category="success")
            return render_template("productcalculation.html",
                                   products=products,
                                   reactants=reactants,
                                   reactionMatrix=key_matrix,
                                   reactionMatrixDownload=key_matrix,
                                   speciesList=species_list,
                                   formatedKeyMatrix=formatted_key_matrix)
    return render_template("productcalculation.html",
                           reactionMatrix="none")


@views.route("/listcalc", methods=["GET", "POST"])
def list_calculation():
    if request.method == "POST":

        species = request.form.get("species")
        species_list = df.create_list(species)
        no_atoms = mc.molecule_checker(species_list)

        if len(species) < 1:
            flash('There are no molecules in the List', category="error")
            return render_template("listcalculation.html",
                                   reactionMatrix="none")

        elif len(no_atoms) > 0:
            alert_string = "The following entries are no atoms: "
            atom_string = ", ".join(no_atoms)
            alert_string += atom_string
            flash(alert_string, category="error")
            return render_template("listcalculation.html",
                                   species=species,
                                   reactionMatrix="none")

        else:
            key_matrix = krc.calc(species_list)
            formatted_key_matrix = df.array_to_matrix(key_matrix, species_list)
            flash('Calculation successful', category="success")
            return render_template("listcalculation.html",
                                   species=species,
                                   reactionMatrix=key_matrix,
                                   reactionMatrixDownload=key_matrix,
                                   speciesList=species_list,
                                   formatedKeyMatrix=formatted_key_matrix)

    return render_template("listcalculation.html",
                           reactionMatrix="none")


@views.route("/download", methods=["POST"])
def download_file():
    if request.method == "POST":
        species = request.form.get("downloadSpeciesList")
        key_matrix = request.form.get("downloadReactionMatrix")
        species_list = df.create_list(species)
        key_matrix_list = df.create_list_for_array(key_matrix)
        download_matrix = [1]
        download_matrix[0] = species_list
        download_matrix += key_matrix_list

        return excel.make_response_from_array(download_matrix, "xlsx", file_name="reaction_matrix")
